import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  asset,
  staticResourceURL,
  Environment,
} from 'react-360';
import EventEmitter from "EventEmitter";
import MediaAppTemplateScenePage from "MediaAppTemplateScenePage.react";
import MediaAppTemplateSubtitleText from "MediaAppTemplateSubtitleText.react";

// The mock database
const SCENE_DEF = [
  {
    type: 'photo',
    title: 'Welcome',
    source: {uri: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2FJupiterTemple360.jpg?alt=media&token=9e44d127-b394-480e-ad3d-e38ddf3a245d'},
    audio: {uri: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2Fsounds%2F28239__herbertboland__forestbirds.wav?alt=media&token=58953b65-5909-49ea-a79a-4f8d49e4cbe8'},
    next: 1,
    subtitle: 'Walk towards the Temple',
  },
{
    type: 'photo',
    title: 'Order of the Temple',
    source: {uri: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2F360_luxor.jpg?alt=media&token=4cda6132-30e7-447a-a03f-5a0388ffc99c'},
    audio: {uri: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2Fsounds%2F459977__florianreichelt__soft-wind.mp3?alt=media&token=8bd6f9c4-090f-4cba-9103-3f49675c98db'},
    next: 2,
    subtitle: 'Enter the Temple',
  },
  {
    type: 'photo',
    title: 'The Temple',
    source: {uri: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2Fchess-world.jpg?alt=media&token=93c84dc0-66f0-4617-ab42-a81572535346'},
    audio: {ur: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2Fsounds%2F29589__erh__angels.wav?alt=media&token=0c941328-4852-4dc8-9015-681abe91dbb7'},
    next: 3,
    subtitle: 'Begin initiation.',
  },
  {
    type: 'photo',
    title: 'The Matrix',
    source: {uri: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2F360_world.jpg?alt=media&token=be61e73f-f59c-4f03-8126-55fa4f0ac2f3'},
    audio: {ur: 'https://firebasestorage.googleapis.com/v0/b/order-of-the-temple.appspot.com/o/static_assets%2Fsounds%2Fmatrix.wav?alt=media&token=5849e5a1-b7fa-4835-bce9-632bc7796646'},
    next: 0,
    subtitle: 'Do anything.',
  },
];

// To share data between different root views, the best way is to
// use data frameworks such as flux or redux.
// Here we just use a simple event emitter.
const dataStore = new EventEmitter();

// The root react component of the app main surface
export default class MediaAppTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    };
  }

  _onClickNext = () => {
    const nextID = SCENE_DEF[this.state.index].next;
    this.setState({index: nextID});
    dataStore.emit('dataChange', nextID);
  };

  render() {
    const currentScene = SCENE_DEF[this.state.index];
    const nextScene = SCENE_DEF[SCENE_DEF[this.state.index].next];
    return (
      <View style={styles.panel}>
        <MediaAppTemplateScenePage
          currentScene={currentScene}
          nextScene={nextScene}
          onClickNext={this._onClickNext} />
      </View>
    );
  }
};

// The root react component of the subtitle surface
export class MediaAppTemplateSubtitle extends React.Component {
  state = {
    index: 0,
  };

  componentWillMount() {
    dataStore.addListener('dataChange', this._onDataChange);
  }
  componentWillUnmount() {
    dataStore.removeListener('dataChange', this._onDataChange);
  }
  _onDataChange = (index) => {
    this.setState({index: index});
  };
  render() {
    const currentScene = SCENE_DEF[this.state.index];
    return (
      <View style={styles.subtitle}>
        <MediaAppTemplateSubtitleText text={currentScene.subtitle} />
      </View>
    );
  }
};

// defining StyleSheet
const styles = StyleSheet.create({
  panel: {
    width: 1000,
    height: 600,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  subtitle: {
    width: 600,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    top: 600,
  },
});

// register the root component
// this will be used from client.js by r360.createRoot('MediaAppTemplate' ...)
AppRegistry.registerComponent('MediaAppTemplate', () => MediaAppTemplate);

// register another root component
// this will be used from client.js by r360.createRoot('MediaAppTemplate' ...)
AppRegistry.registerComponent('MediaAppTemplateSubtitle', () => MediaAppTemplateSubtitle);
